#include <stdio.h>
#include <string.h>

#include "util.h"

int queryLine(char *msg, char *buf, int n) {
    
    printf("%s", msg);

    if (fgets(buf, n, stdin) == NULL) {
        return 0;
    } else {
        // Remove trailing newline
        int l = strlen(buf);
        if (buf[l-1] == '\n')
            buf[l-1] = '\0';

        return 1;
    }
}
