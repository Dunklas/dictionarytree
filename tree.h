#ifndef TREE_HEADER
#define TREE_HEADER

/*
 * Represents a node in a dictionary tree
 */
typedef struct treenode_ {
    
    unsigned int frequency;
    char character;

    struct treenode_ *left, *right;

} treenode;

typedef treenode *treenodeptr;

#endif
