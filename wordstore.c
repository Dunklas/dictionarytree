#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "wordstore.h"

int buildWordStore(FILE *fp, wordstore **s) {

    // Temporary buffer for each word read from input file
    char buf[1024];

    // Indicates if we're currently inside a word or not (while reading input)
    _Bool inWord = 0;

    // Keeps track of length of currently read word
    unsigned int wordLen = 0;

    // Allocate memory for the actual wordstore struct
    if ((*s = (wordstore *) malloc(sizeof(wordstore))) == NULL) {
        return 0;
    }

    // Init wordstore struct
    (*s)->store = NULL;
    (*s)->size = 0;

    int c;
    while ((c = fgetc(fp)) != EOF) {
        if (isalnum(c)) { // Part of word
            if (!inWord) {
                inWord = 1; // First char of word, change state
            }
            buf[wordLen++] = tolower(c); // Copy value to buffer

        } else { // Not part of word
            if (inWord) {
                inWord = 0; // Change state
                buf[wordLen] = '\0'; // Add terminating null

                char **tmpStore; // Use temp pointer (otherwise we can't free up memory if a malloc/realloc fails
                if ((*s)->store == NULL) { // NULL means this is the first word, therefore use malloc
                    if ((tmpStore = (char **) malloc(sizeof(char *))) == NULL) {
                        free(*s);
                        return 0;
                    } 
                } else { // Not NULL means this is not the first word, therefore use realloc
                    if ((tmpStore = (char **) realloc((*s)->store, ((*s)->size+1) * sizeof(char *))) == NULL) {
                        free((*s)->store);
                        free(*s);
                        return 0;
                    }
                }
                (*s)->store = tmpStore; // (*s)->store can now fit current word (thanks to above malloc/realloc!)

                char *tmpWord; // New temp pointer for the actual word
                if ((tmpWord = (char *) malloc((wordLen+1) * sizeof(char))) == NULL) {

                    for (int i = 0; i < (*s)->size; i++) {
                        free((*s)->store[i]); // We might have stored words previously, free that memory!
                    }

                    free((*s)->store);
                    free(*s);

                    return 0;
                }
                (*s)->store[(*s)->size] = tmpWord;
                strncpy((*s)->store[(*s)->size++], buf, wordLen+1);
                
                // Now, the current word is stored into (*s)->store[x]

                wordLen = 0; // Reset length for next word (if any)
            }
        }
    }

    return 1;
}

void clearWordStore(wordstore **s) {

    for (int i = 0; i < (*s)->size; i++) {
        free((*s)->store[i]);
    }

    free((*s)->store);
    free(*s);

}
