#ifndef WORDSTORE_HEADER
#define WORDSTORE_HEADER

/*
 * A struct for storing multiple words.
 * Size property indicates how many words there are stored.
 */
typedef struct {

    char **store;
    unsigned long size;

} wordstore;

/*
 * Builds a wordstore from a given text file.
 * Definition of a word in this case is any sequence
 * of a-z, A-Z, 0-9. Returns 1 if successful, or 0
 * if memory allocation failed.
 *
 * @param fp    a file pointer to a text file containing words
 * @param s     a pointer to a wordstore pointer, where wordstore will be stored
 */
int buildWordStore(FILE *fp, wordstore **s);

/*
 * Frees memory allocated for a wordstore.
 *
 * @param s     a pointer to a wordstore pointer, which points at the wordstore to be freed
 */
void clearWordStore(wordstore **s);

#endif
