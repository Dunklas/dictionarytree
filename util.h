#ifndef UTIL_HEADER
#define UTIL_HEADER

/*
 * Prints msg and reads n characters from stdin
 * to buf. Returns 0 if no characters were entered (EOF),
 * or 1 if input was queried successfully.
 *
 * @param msg   a message to be printed before query for input
 * @param buf   a char pointer where input is stored
 * @param n     max amount of characters to be read
 */
int queryLine(char *msg, char *buf, int n);

#endif
