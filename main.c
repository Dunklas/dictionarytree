#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "wordstore.h"
#include "util.h"
#include "tree.h"

#define F_MAX 256

int cstrcmp(const void *a, const void *b);

treenodeptr make_treenode(char c);
treenodeptr insert(const char *word, unsigned int index, treenodeptr node);
void ascending_write(treenodeptr node, char wordbuf[], unsigned int wordlen);
void free_tree(treenodeptr node);

int main(void) {

    char fname[F_MAX];
    FILE *fp = NULL;
    wordstore *s;

    if (!queryLine("Please enter filename: ", fname, F_MAX)) {
        printf("\n");
        exit(0);
    }

    fp = fopen(fname, "r");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file.\n");
        exit(1);
    }
        
    if (!buildWordStore(fp, &s)) {
        fprintf(stderr, "Could not allocate memory for words.\n");
        fclose(fp);
        exit(1);
    }

    if (s->size <= 0) {
        fprintf(stderr, "Input file does not contain any valid words.\n");
        clearWordStore(&s);
        exit(1);
    } 

    // Sort words in wordstore
    qsort(s->store, s->size, sizeof(char *), cstrcmp);

    // Insert each word into a tree
    treenodeptr root = NULL;
    unsigned int maxSize = 0;
    for (int i = 0; i < s->size; i++) {
        root = insert(s->store[i], 0, root);
     
        // While we're looping the words...get size of longest word   
        unsigned int length = strlen(s->store[i]);
        if (length > maxSize) {
            maxSize = length;
        }
    }
    
    char tmpbuf[maxSize]; // Create buffer which can fit longest word (without terminating null)
    printf("Word\tFrequency\n");
    ascending_write(root, tmpbuf, 0); // Print content of tree!

    // Free resources
    free_tree(root);
    clearWordStore(&s);
    fclose(fp);
    return (0);
}

// Compare method for qsort
int cstrcmp(const void *a, const void *b) {
    const char *ia = *(const char **) a;
    const char *ib = *(const char **) b;
    return strcmp(ia, ib);
}

// Creates a node for char c
treenodeptr make_treenode(char c) {
    treenodeptr new_node;

    new_node = (treenode *) malloc(sizeof(treenode));
    if (new_node == NULL) {
        // If no memory is available here, there may be a leak..BUT the program will exit and therefore I won't bother...
        fprintf(stderr, "Could not allocate memory for tree node.\n");
        exit(-1);
    }
    new_node->character = c;
    new_node->frequency = 0;
    new_node->left = NULL;
    new_node->right = NULL;

    return (treenodeptr) new_node;
}

/*
 * Inserts word into a tree.
 * Index is used to keep track of what char of word
 * that we are currently investigating.
 */
treenodeptr insert(const char *word, unsigned int index, treenodeptr node) {

    // Create node if null
    if (node == NULL) {
        node = make_treenode(word[index]);
    }  

    // This node represents current character, but current word is longer so we need to go deeper (left) into the tree
    if (word[index] == node->character &&
            index < strlen(word)-1) {
        node->left = insert(word, index+1, node->left);

    // This node does not represent current character, go right to see if we can find one that does (or create a new node)
    } else if (word[index] != node->character) {
        node->right = insert(word, index, node->right);
    }

    // This node represents current character and we have reached the end of the word. Increment frequency of this node
    if (word[index] == node->character &&
            index == strlen(word)-1) {
        node->frequency++;
    }
        
    return (treenodeptr) node;

}

/*
 * Prints the content of a dictionary tree.
 * wordbuf is a buffer where current word to be printed is stored.
 * wordlen keeps track of length of current word.
 */
void ascending_write(treenodeptr node, char wordbuf[], unsigned int wordlen) {

    if (node == NULL)
        return;

    // Copy current char to buffer
    wordbuf[wordlen] = node->character;

    // If positive frequency, print word, frequency and \n
    if (node->frequency > 0) {
        for (int i = 0; i < wordlen+1; i++) {
            putchar(wordbuf[i]);
        }
        printf("\t%d\n", node->frequency);
    }

    // If there is a left branch, it means we have a longer word to print from this branch. Do recursive call and increment wordlen
    if (node->left != NULL) {
        ascending_write(node->left, wordbuf, wordlen+1);
    }

    // If there is a right branch, it means we have more words on the "same level". Do recursive call with same wordlen (since we don't want to print current char)
    if (node->right != NULL) {
        ascending_write(node->right, wordbuf, wordlen);
    }
}

/*
 * Free resources for tree.
 */
void free_tree(treenodeptr node) {
    if (node->left != NULL) {
        free_tree(node->left);
    }
    if (node->right != NULL) {
        free_tree(node->right);
    }

    free(node);
}
